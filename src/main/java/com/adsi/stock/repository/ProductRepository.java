package com.adsi.stock.repository;

import com.adsi.stock.domain.Product;
import com.adsi.stock.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, String> {
}
