package com.adsi.stock.service.dto;

import com.adsi.stock.domain.Product;
import com.adsi.stock.domain.Users;

public class ProductTransformer {

    public static ProductDTO getProductDTOFromProduct(Product product){
        if(product == null){
            return null;
        }

        ProductDTO dto = new ProductDTO();

        dto.setReference(product.getReference());
        dto.setName(product.getName());

        return dto;
    }

    public static Product getProductFromProductDTO(ProductDTO dto){
        if(dto == null){
            return null;
        }

        Product product = new Product();

        product.setReference(dto.getReference());
        product.setName(dto.getName());

        return product;
    }
}
