package com.adsi.stock.service.dto;

import com.adsi.stock.domain.Rols;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class UsersDTO {

    @Id
    private Long id;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 4, max = 20, message = "El tamaño del campo debe ser entre 4 y 20 caracteres")
    private String username;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 2, max = 40, message = "El tamaño del campo debe ser entre 4 y 20 caracteres")
    private String name;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 2, max = 40, message = "El tamaño del campo debe ser entre 4 y 20 caracteres")
    private String lastName;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Email(message = "El email no tiene la estructura correcta")
    private String email;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Column(length = 60)
    private String password;

    private Boolean enabled;

    private List<Rols> rols;
}
