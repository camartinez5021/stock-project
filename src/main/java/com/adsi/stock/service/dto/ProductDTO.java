package com.adsi.stock.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
public class ProductDTO {

    @Id
    private String reference;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 2, max = 20, message = "El tamaño del campo debe ser entre 2 y 20 caracteres")
    @Column(length = 20)
    private String name;
}
