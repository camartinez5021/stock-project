package com.adsi.stock.service;

import com.adsi.stock.service.dto.ProductDTO;
import com.adsi.stock.service.dto.UsersDTO;
import org.springframework.data.domain.Page;

public interface IProductService {
    Page<ProductDTO> getAll(Integer pageNumber, Integer pageSize);
    ProductDTO create(ProductDTO productDTO);
    ProductDTO getById(String reference);
}
