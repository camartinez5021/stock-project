package com.adsi.stock.service.imp;

import com.adsi.stock.domain.Product;
import com.adsi.stock.domain.Users;
import com.adsi.stock.repository.ProductRepository;
import com.adsi.stock.repository.UserRepository;
import com.adsi.stock.service.IProductService;
import com.adsi.stock.service.dto.ProductDTO;
import com.adsi.stock.service.dto.ProductTransformer;
import com.adsi.stock.service.dto.UserTransformer;
import com.adsi.stock.service.dto.UsersDTO;
import com.adsi.stock.service.error.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IProductServiceImp implements IProductService {

    @Autowired
    ProductRepository repository;

    @Override
    public Page<ProductDTO> getAll(Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return repository.findAll(pageable)
                .map(ProductTransformer::getProductDTOFromProduct);
    }

    @Override
    public ProductDTO create(ProductDTO productDTO) {
        ProductTransformer.getProductDTOFromProduct(repository.save(ProductTransformer.getProductFromProductDTO(productDTO)));
        return null;
    }

    @Override
    public ProductDTO getById(String reference) {
        Optional<Product> product = repository.findById(reference);
        if(!product.isPresent()){
            throw new ObjectNotFoundException("Error: El product con referencia " + reference + " no existe");
        }
        return product
                .map(ProductTransformer::getProductDTOFromProduct)
                .get();
    }
}
