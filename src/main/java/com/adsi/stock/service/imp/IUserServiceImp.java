package com.adsi.stock.service.imp;

import com.adsi.stock.domain.Users;
import com.adsi.stock.repository.UserRepository;
import com.adsi.stock.service.IUserService;
import com.adsi.stock.service.dto.UserTransformer;
import com.adsi.stock.service.dto.UsersDTO;
import com.adsi.stock.service.error.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IUserServiceImp implements IUserService {

    @Autowired
    UserRepository repository;

    @Override
    public Page<UsersDTO> getAll(Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return repository.findAll(pageable)
                .map(UserTransformer::getUsersDTOFromUsers);
    }

    @Override
    public UsersDTO create(UsersDTO usersDTO) {
        UserTransformer.getUsersDTOFromUsers(repository.save(UserTransformer.getUsersFromUsersDTO(usersDTO)));
        return null;
    }

    @Override
    public UsersDTO getById(Long id) {
        Optional<Users> user = repository.findById(id);
        if(!user.isPresent()){
            throw new ObjectNotFoundException("Error: No el id " + id + " ingresado no existe");
        }
        return user
        .map(UserTransformer::getUsersDTOFromUsers)
                .get();
    }
}
