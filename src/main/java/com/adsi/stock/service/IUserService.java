package com.adsi.stock.service;

import com.adsi.stock.domain.Users;
import com.adsi.stock.service.dto.UsersDTO;
import org.springframework.data.domain.Page;

public interface IUserService {
    Page<UsersDTO> getAll(Integer pageNumber, Integer pageSize);
    UsersDTO create(UsersDTO usersDTO);
    UsersDTO getById(Long id);
}
