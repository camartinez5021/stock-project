package com.adsi.stock.auth;

public class JwtConfig {

    public static final String RSA_private = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEowIBAAKCAQEAwTkatutq1j926FZiqGcFLRZMCe82DaNYmkglG34kdbqNEQnf\n" +
            "bOZKyk6H6wX56xoDQnKgfMqfiPozvS0xqkyRMIDBO0YJ+0Jb5OiNKMk458OUut2x\n" +
            "LihWRZ5t1P6rYpoPMByprgJ8kcuMRPwgYT2xZwUETEm1YdYBVmZYHQIVJvSy/jvM\n" +
            "Bk80BTcOMh4tG1uIoh9VP/iagflUmQHJUx2Sy6X7wAP1Ug76rXHKWbuo4i0vF5Iz\n" +
            "VOSp3KkSn63uCf3qmav602VTngCP770p0bqjvwnNFomj34D6oWNMHAmpmafyiKTu\n" +
            "dl/Xky+NnTmaj0nogecvmdiPF3tScW7ySkRirwIDAQABAoIBAQCiHlJ+xVk3d7pB\n" +
            "yDEw0Hc0VC/qae11VedCU2PnBFh8+s/fSuROT47S3T8G/1WoX4P6nYhwC+X9HMiK\n" +
            "Yj/4fhOXmYAHRoWbbhsCHehKH0MCmkPQ4a8GZku0T/AhNKgBq/cwS+xGFsoD3b/M\n" +
            "JarZS9Tr9facN3/bHZ6zql0lwZiTvGd9ufDN6oJ7x/z8o9sHm35OnaKGzGwu5hkw\n" +
            "qhYKTO32kn1TVYcv+lBw2ojKOh4GfkS81oCLQdzk2FN31PrQTkJUJ5ML4UD7aide\n" +
            "pHOqseV0/4dykBGU+vzwduELrkHBMr1dNdjgIjz+2zMZkOyDmHW1gcqtumxrq6lB\n" +
            "EzSXMTBJAoGBAPEoyln8FGXkCz/Hc/sYoU3JZvOol9fJdXuAuMFnx46KPcgdSjxm\n" +
            "+KkH8rFhtQZ6VS81zURngva/VyaCArHpFifQzn6NcVuLaj+j7MFmlpXC3uqsTFHA\n" +
            "CLn7fYVDs/j+D3VebjS75RYbZNJJa2dnMjsLoIUfUibdZa8POiuQB3B7AoGBAM0d\n" +
            "IPcrN9jU4Hlxi+43tzCsaHdldwwbMKyr+s2xqEfZNinNwh+T3bC7i7TpK5EDkuBH\n" +
            "ZXCIQlV45BD1vqCWNDe0YEoe2n51mSBtgNBDE43nb50YvqUHywojO6lmcAZbpxiB\n" +
            "YpYAR/UW3Edx3LmeWcW6NqsTp0VJLE/CmDHKobJdAoGAPnxYOgqj6ma1N3hhp90Q\n" +
            "5ovZ3iZ/+YycoTyQIqTWQyu0qGZWduIjagDCNI3hjuUhr1Lcde3wNCkN1s93DUwc\n" +
            "zoI/DvFtPtAX3peF3/LJ79IHWnz8ZW5BlJcCRgZt2oVR0hJ+sbFBwYGllQsrBibq\n" +
            "D/yKQhQEgEd7cmmao/D0zIMCgYAibVkzI1XkVxa2S5CpE0D0Mnv2TlmE9qtc0CDw\n" +
            "lXkhVhv5OEf09OSo3VG9s8VUOb1VV6Z3FOfNYNUm45hza8Kn9uaSiksyHprtDnQo\n" +
            "gJmvn8DkJVsaaAx0TyWccU0sRAXv3welFbb+NDAU7c+WeDWpMnAqlsnknzuIx6iz\n" +
            "8sbhIQKBgHFJMrq24ss1WmI3nwih216Y2EvYH6F4ilTnU6QZC6pVG4ukctOf3lr7\n" +
            "7z/JQkmg8bX+QU2aJsZiKqLIh4M5kbmGT2oi7E1K68hMSqocbCOp0xkBhv+ZPm27\n" +
            "Q/aOgvpNS62x7DHUqrTDOFoBKpETjI083892nEEiQosTTNxEozh5\n" +
            "-----END RSA PRIVATE KEY-----";

    public static final String RSA_PUBLIC = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwTkatutq1j926FZiqGcF\n" +
            "LRZMCe82DaNYmkglG34kdbqNEQnfbOZKyk6H6wX56xoDQnKgfMqfiPozvS0xqkyR\n" +
            "MIDBO0YJ+0Jb5OiNKMk458OUut2xLihWRZ5t1P6rYpoPMByprgJ8kcuMRPwgYT2x\n" +
            "ZwUETEm1YdYBVmZYHQIVJvSy/jvMBk80BTcOMh4tG1uIoh9VP/iagflUmQHJUx2S\n" +
            "y6X7wAP1Ug76rXHKWbuo4i0vF5IzVOSp3KkSn63uCf3qmav602VTngCP770p0bqj\n" +
            "vwnNFomj34D6oWNMHAmpmafyiKTudl/Xky+NnTmaj0nogecvmdiPF3tScW7ySkRi\n" +
            "rwIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
