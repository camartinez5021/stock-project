package com.adsi.stock.web.rest;

import com.adsi.stock.service.IUserService;
import com.adsi.stock.service.dto.UsersDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class UserResource {

    @Autowired
    IUserService service;

    @GetMapping("/user")
    public Page<UsersDTO> getAll(@RequestParam(value = "page") Integer pageNumber,
                                 @RequestParam(value = "size") Integer pageSize){
        return service.getAll(pageNumber, pageSize);

    }

    @PostMapping("/user")
    public ResponseEntity<?> create(@Valid @RequestBody UsersDTO usersDTO, BindingResult result){
        UsersDTO dto = null;
        Map<String, Object> response = new HashMap<>();
        if(result.hasErrors()) {
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(e -> "El campo " + e.getField() + " " + e.getDefaultMessage())
                    .collect(Collectors.toList());
            response.put("error", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try{
            dto = service.create(usersDTO);
        }catch (DataAccessException err){
            response.put("message", "Error al crear un usuario");
            response.put("error", err.getMessage() + ": " +
                    err.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("message", "El usuario ha sido creado correctamente");
        response.put("User", dto);
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<UsersDTO> getById(@PathVariable Long id){
        return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
    }
}
