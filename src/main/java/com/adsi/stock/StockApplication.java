package com.adsi.stock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class StockApplication implements CommandLineRunner {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(StockApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		String password = "1d2e3v4e5l6o7p";
		for (int i=0; i<4; i++){
			String passwordBCry = passwordEncoder.encode(password);
			System.out.println(passwordBCry);
		}
	}
}
