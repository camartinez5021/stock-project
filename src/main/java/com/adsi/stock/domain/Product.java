package com.adsi.stock.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product implements Serializable {

    @Id
    private String reference;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 2, max = 20, message = "El tamaño del campo debe ser entre 2 y 20 caracteres")
    @Column(length = 20)
    private String name;
}
