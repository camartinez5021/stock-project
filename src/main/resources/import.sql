INSERT INTO users (email, last_name, name, password, username, enabled) VALUES ('abivan@gmail.com', 'agudelo', 'ivan', '$2a$10$g1nVTAUXwgoM7uyTrTN.8.iIV/IwEwT7m2Np4VTv8FW6X2T8tIMgK', 'ivan', true);
INSERT INTO users (email, last_name, name, password, username, enabled) VALUES ('fabian@gmail.com', 'rojas', 'fabian', '$2a$10$g1nVTAUXwgoM7uyTrTN.8.iIV/IwEwT7m2Np4VTv8FW6X2T8tIMgK', 'fabian', true);
INSERT INTO users (email, last_name, name, password, username, enabled) VALUES ('yohon@gmail.com', 'bravo', 'yohon', '$2a$10$g1nVTAUXwgoM7uyTrTN.8.iIV/IwEwT7m2Np4VTv8FW6X2T8tIMgK', 'yohon', true);
INSERT INTO users (email, last_name, name, password, username, enabled) VALUES ('freddy@gmail.com', 'sierra', 'freddy', '$2a$10$g1nVTAUXwgoM7uyTrTN.8.iIV/IwEwT7m2Np4VTv8FW6X2T8tIMgK', 'freddy', true);

INSERT INTO rols VALUES (1, 'ROLE_USER');
INSERT INTO rols VALUES (2, 'ROLE_ADMIN');

INSERT INTO user_has_rol VALUES (1, 1), (1, 2);
INSERT INTO user_has_rol VALUES (2, 1);
INSERT INTO user_has_rol VALUES (3, 2);

INSERT INTO product (reference, name) VALUES ('1', 'Televisor'), ('2', 'Computador');

